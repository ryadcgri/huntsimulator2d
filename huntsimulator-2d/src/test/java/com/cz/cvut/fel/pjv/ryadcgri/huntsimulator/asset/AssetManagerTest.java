package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset;

import static org.junit.jupiter.api.Assertions.*;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Gorilla;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.MON_GreenSlime;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.MON_Wolf;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC_Merchant;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC_OldMan;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment.OBJ_Axe;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment.OBJ_Shield_Blue;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.inventory.OBJ_Potion_Red;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Ammo;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Coin_Bronze;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Heart;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.TileManager;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.interactive.IT_DryTree;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.interactive.InteractiveTile;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Gorilla;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.MON_GreenSlime;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.MON_Wolf;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Monster;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC_Merchant;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC_OldMan;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.interactive.IT_DryTree;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.interactive.InteractiveTile;
import com.google.gson.Gson;
import com.google.gson.JsonArray;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import java.io.FileReader;
import java.util.Map;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

class AssetManagerTest {

    @Mock
    private GamePanel gamePanel = new GamePanel();

    @InjectMocks
    private AssetManager assetManager = new AssetManager(gamePanel);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this);
    }

    @Test
    void testSetObjects() throws Exception {
        // Load JSON data
        JsonObject entities = loadEntitiesJson();

        // Mock method calls
        when(gamePanel.getTileSize()).thenReturn(1);

        // Invoke method
        assetManager.setObjects();

        // Assertions
        Asset[] objectsArray = gamePanel.getObjects()[0];
        assertEquals(8, objectsArray.length);
        assertEquals(OBJ_Coin_Bronze.class, objectsArray[0].getClass());
        assertEquals(OBJ_Axe.class, objectsArray[3].getClass());
        assertEquals(OBJ_Potion_Red.class, objectsArray[5].getClass());

    }

    @Test
    void testSetNPCs() throws Exception {
        // Load JSON data
        JsonObject entities = loadEntitiesJson();

        // Invoke method
        assetManager.setNPCs();

        // Assertions
        Asset[] npcsArray = gamePanel.getNpcs()[0];
        assertEquals(0, npcsArray.length);
    }

    @Test
    void testSetMonsters() throws Exception {
        // Load JSON data
        JsonObject entities = loadEntitiesJson();

        // Mock method calls if needed
        // when(gamePanel.getTileSize()).thenReturn(1);

        // Invoke method
        assetManager.setMonsters();

        // Assertions
        Asset[] monstersArray = gamePanel.getMonsters()[0];
        assertEquals(7, monstersArray.length);
        assertEquals(MON_GreenSlime.class, monstersArray[0].getClass());
        assertEquals(MON_Wolf.class, monstersArray[1].getClass());
        assertEquals(Gorilla.class, monstersArray[6].getClass());
    }

    @Test
    void testSetInteractiveTiles() throws Exception {
        // Load JSON data
        JsonObject entities = loadEntitiesJson();

        // Mock method calls if needed
        // when(gamePanel.getTileSize()).thenReturn(1);

        // Invoke method
        assetManager.setInteractiveTiles();

        // Assertions
        InteractiveTile[] tilesArray = gamePanel.getInteractiveTiles()[0];
        assertEquals(19, tilesArray.length);
        assertEquals(IT_DryTree.class, tilesArray[0].getClass());
    }

    private JsonObject loadEntitiesJson() throws Exception {
        JsonParser parser = new JsonParser();
        JsonElement jsonElement = parser.parse(new FileReader("src/main/resources/assets/entities.json"));
        return jsonElement.getAsJsonObject();
    }

}

