package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile;

import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.player.Player;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

class TileManagerTest {

    @Mock
    private GamePanel gamePanel = new GamePanel();

    @InjectMocks
    private TileManager tileManager = new TileManager(gamePanel, 50);

    @BeforeEach
    void setUp() {
        MockitoAnnotations.initMocks(this); // Use initMocks instead of openMocks
        when(gamePanel.getMaxMaps()).thenReturn(2);
        when(gamePanel.getMaxWorldColumns()).thenReturn(50);
        when(gamePanel.getMaxWorldRows()).thenReturn(50);
        when(gamePanel.getTileSize()).thenReturn(32);
    }

    @Test
    void testInitialization() {
        assertNotNull(tileManager.getTiles());
        assertEquals(50, tileManager.getTiles().length);  // Assuming dimensions parameter is 50
        assertNotNull(tileManager.getMapTileNumbers());
        assertEquals(10, tileManager.getMapTileNumbers().length);  // maxMaps is 2
    }

    @Test
    void testGetTileImage() {
        tileManager.getTileImage();
        for (int i = 0; i < 10; i++) {
            assertNotNull(tileManager.getTiles()[i].getImage());
        }
        for (int i = 10; i < 20; i++) {
            assertNotNull(tileManager.getTiles()[i].getImage());
        }
        int ten = 10;
            assertFalse(tileManager.getTiles()[ten].isCollision());

        // Add more assertions as needed based on the tile setup
    }

    @Test
    void testLoadMapFromJson() throws Exception {
        String json = "{\"map\":[[0,1,2],[3,4,5],[1,1,1]}";
        InputStream inputStream = new java.io.ByteArrayInputStream(json.getBytes());
        //when(gamePanel.getClass().getResourceAsStream("/maps/worldinjson.json")).thenReturn(inputStream);

        tileManager.loadMapFromJson("/maps/worldinjson.json", 0);

        int[][][] mapTileNumbers = tileManager.getMapTileNumbers();
        assertEquals(12, mapTileNumbers[0][0][0]);
        assertEquals(12, mapTileNumbers[0][1][0]);
        assertEquals(12, mapTileNumbers[0][2][0]);
        assertEquals(12, mapTileNumbers[0][0][1]);
        assertEquals(12, mapTileNumbers[0][1][1]);
        assertEquals(12, mapTileNumbers[0][2][1]);
    }

    @Test
    void testDraw() {
        Graphics2D graphics2D = mock(Graphics2D.class);
        when(gamePanel.getCurrentMap()).thenReturn(0);
        when(gamePanel.getMaxWorldColumns()).thenReturn(2);
        when(gamePanel.getMaxWorldRows()).thenReturn(2);
        when(gamePanel.getPlayer()).thenReturn(mock(Player.class));
        when(gamePanel.getPlayer().getWorldX()).thenReturn(0);
        when(gamePanel.getPlayer().getWorldY()).thenReturn(0);
        when(gamePanel.getPlayer().getScreenX()).thenReturn(100);
        when(gamePanel.getPlayer().getScreenY()).thenReturn(100);

        tileManager.loadMapFromJson("/maps/worldinjson.json", 0);
        tileManager.draw(graphics2D);

        //verify(graphics2D, times(4)).drawImage(any(), anyInt(), anyInt(), isNull());
    }

    //UNIT TESTS

    @Test
    public void testValidMapJsonInput() {

        tileManager.loadMapFromJson("/maps/testMap.json", 2);

        int[][][] expectedMap = {
                {
                        {0, 1, 2},
                        {3, 4, 5},
                        {1, 1, 1}
                }
        };
        assertArrayEquals(expectedMap, tileManager.getMapTileNumbers());
        System.out.println(Arrays.deepToString(tileManager.getMapTileNumbers()));
    }

    @Test
    public void testLoadInvalidJsonMap() {
        String json = "{\"map\":[[0,1,2],[3,4,5],[1,1,1]";  // Missing closing brackets
        InputStream inputStream = new ByteArrayInputStream(json.getBytes());

        //when(gamePanel.getClass().getResourceAsStream("/maps/worldinjson.json")).thenReturn(inputStream);

        assertThrows(IOException.class, () -> {
            tileManager.loadMapFromJson("/maps/testMap.json", 0);
        });
    }

    @Test
    public void testLoadJsonMapWithMissingFields() {
        String json = "{\"map\":[[0,1,2],[3,4,5]]}";  // Missing third row
        InputStream inputStream = new ByteArrayInputStream(json.getBytes());

        //when(gamePanel.getClass().getResourceAsStream("/maps/testMap.json")).thenReturn(inputStream);

        tileManager.loadMapFromJson("/maps/testMap.json", 0);

        // Verify the map was loaded with missing fields handled appropriately
        int[][][] mapTileNumbers = tileManager.getMapTileNumbers();
        assertNotNull(mapTileNumbers);
        assertEquals(2, mapTileNumbers[0].length); // Only two rows
        assertEquals(3, mapTileNumbers[0][0].length); // Three columns
        assertEquals(3, mapTileNumbers[0][1].length); // Three columns
    }

}
