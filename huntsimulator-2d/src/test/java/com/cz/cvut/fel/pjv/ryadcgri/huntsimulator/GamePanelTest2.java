package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator;

import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.Asset;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Gorilla;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.Object;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.player.Player;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.CollisionChecker;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.KeyHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class GamePanelTest2 {

    private GamePanel gamePanel;
    private Player player;

    @BeforeEach
    public void setUp() {
        gamePanel = new GamePanel();
        player = new Player(gamePanel, new KeyHandler(gamePanel));
        player.setDefaultValues();
    }

    @Test
    public void testPlayerInitialization() {
        assertNotNull(player);
        assertEquals(400 - 16, player.getScreenX());
        assertEquals(300 - 16, player.getScreenY());
    }

    @Test
    public void testSetDefaultValues() {
        assertEquals(4, player.getSpeed());
        assertEquals(6, player.getMaxLife());
        assertEquals(player.getMaxLife(), player.getCurrentLife());
        assertEquals(4, player.getMaxMana());
        assertEquals(player.getMaxMana(), player.getCurrentMana());
        assertEquals(10, player.getMaxAmmo());
        assertEquals(player.getMaxAmmo(), player.getCurrentAmmo());
        assertEquals(1, player.getLevel());
        assertEquals(1, player.getStrength());
        assertEquals(1, player.getDexterity());
        assertEquals(0, player.getExp());
        assertEquals(5, player.getNextLevelExp());
        assertEquals(0, player.getCoins());
        assertEquals(player.getAttack(), player.getAttackPower());
        assertEquals(player.getDefense(), player.getDefensePower());
    }

    @Test
    public void testSetItems() {
        player.setItems();
        assertEquals(3, player.getInventory().size());
        assertTrue(player.getInventory().contains(player.getCurrentWeapon()));
        assertTrue(player.getInventory().contains(player.getCurrentShield()));
    }

    @Test
    public void testRestoreLifeAndMana() {
        player.setCurrentLife(3);
        player.setCurrentMana(2);
        player.setInvincible(true);

        player.restoreLifeAndMana();

        assertEquals(player.getMaxLife(), player.getCurrentLife());
        assertEquals(player.getMaxMana(), player.getCurrentMana());
        assertFalse(player.isInvincible());
    }

    @Test
    public void testDamageMonster() {
        // Create a new instance of a monster
        Gorilla monster = new Gorilla(gamePanel);

        // Get the initial health of the monster
        int initialHealth = monster.getCurrentLife();

        // Damage the monster using the player's method
        player.damageMonster(10, 10);

        // Verify that the monster's health has decreased
        int updatedHealth = monster.getCurrentLife();
        assertEquals(initialHealth - 10, updatedHealth);
    }

    @Test
    public void testPickUpObject() {
        // Create a new instance of an object to pick up
        Object object = new Object(gamePanel);
        Asset [][] assets;

        // Add the object to the game panel or player's inventory
        gamePanel.getPlayer().getInventory().add(object);
        // Check if the object is now in the game panel's object list
        assertTrue(gamePanel.getObjects().equals(object));
    }

    @Test
    public void testUpdate() {
        // Set up the initial player state (e.g., position, health, mana, etc.)
        int initialX = player.getWorldX();
        int initialY = player.getWorldY();

        // Simulate a key press that should move the player (e.g., right arrow)
        gamePanel.getKeyHandler().getRightPressed();

        // Call the update method to process the movement
        player.update();

        // Verify that the player's position has changed accordingly
        int updatedX = player.getWorldX();
        int updatedY = player.getWorldY();
        assertNotEquals(initialX, updatedX);
        assertEquals(initialY, updatedY); // Assuming movement only occurs horizontally
    }
}
