package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator;

import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.Entity;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Gorilla;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.Object;
import junit.framework.TestCase;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.mock;

public class GamePanelTest extends TestCase {

    @Test
    public void testDamageMonster() {
        GamePanel gamePanel = new GamePanel();
        gamePanel.setUpGame();

        // Mock a monster
        Gorilla monsterMock = mock(Gorilla.class);

        // Assuming there's a method to damage a monster
        // For example purposes, let's say there's a method in GamePanel to damage a monster
        // and it decrements the monster's health
        gamePanel.getPlayer().damageMonster(6, 2);

        // Assert that the monster's health has decreased by 2
        assertEquals(monsterMock.getCurrentLife(), monsterMock.getMaxLife());
    }

    @Test
    public void testPickUpObject() {
        GamePanel gamePanel = new GamePanel();
        gamePanel.setUpGame();

        // Mock an object
        Object objectMock = mock(Object.class);

        // Assuming there's a method to pick up an object
        // For example purposes, let's say there's a method in GamePanel to pick up an object
        gamePanel.getPlayer().pickUpObject(6);

        // Assert that the object is in the player's inventory
        assertEquals(gamePanel.getPlayer().getInventory().size(), 3);
        assertEquals(gamePanel.getPlayer().getInventory().get(0), objectMock);
    }




}
