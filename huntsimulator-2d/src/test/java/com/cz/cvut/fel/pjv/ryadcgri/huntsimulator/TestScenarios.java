package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Gorilla;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.player.Player;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment.OBJ_Axe;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment.OBJ_Knife_Normal;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.KeyHandler;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;

public class TestScenarios {

    private Player player;
    private GamePanel gamePanel;
    private Gorilla monster;

    @BeforeEach
    public void setUp() {
        gamePanel = new GamePanel();
        player = new Player(gamePanel, new KeyHandler(gamePanel));
        monster = new Gorilla(gamePanel);
        monster.setCurrentLife(8);  // Initial monster HP
    }

    @Test
    public void testDetailedScenario1() {
        // Initialization
        player.setLevel(1);
        monster.setCurrentLife(8);

        // Action 1: First Bullet
        player.damageMonster(0, 2); // Bullet damage
        assertEquals(6, monster.getCurrentLife());

        // Action 2: Second Bullet
        player.damageMonster(0, 2); // Bullet damage
        assertEquals(4, monster.getCurrentLife());

        // Action 3: Axe
        player.setCurrentWeapon(new OBJ_Axe(gamePanel));
        player.damageMonster(0, 2); // Axe damage at level 1
        assertEquals(2, monster.getCurrentLife());

        // Action 4: Knife
        player.setCurrentWeapon(new OBJ_Knife_Normal(gamePanel));
        player.damageMonster(0, 1); // Knife damage at level 1
        assertEquals(1, monster.getCurrentLife());
    }

    @Test
    public void testDetailedScenario2() {
        // Initialization
        player.setLevel(1);
        monster.setCurrentLife(8);

        // Action 1: First Bullet
        player.damageMonster(0, 2); // Bullet damage
        assertEquals(6, monster.getCurrentLife());

        // Action 2: Second Bullet
        player.damageMonster(0, 2); // Bullet damage
        assertEquals(4, monster.getCurrentLife());

        // Level Up
        player.setLevel(2);

        // Action 3: Axe
        player.setCurrentWeapon(new OBJ_Axe(gamePanel));
        player.damageMonster(0, 3); // Axe damage at level 2
        assertEquals(1, monster.getCurrentLife());

        // Action 4: Knife
        player.setCurrentWeapon(new OBJ_Knife_Normal(gamePanel));
        player.damageMonster(0, 2); // Knife damage at level 2
        assertTrue(monster.getCurrentLife() <= 0); // Monster should be dead
    }
}
