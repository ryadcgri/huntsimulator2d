HUNT SIMULATOR 2D

Application description

The application to be tested is a 2D-hunting simulator game. 
It offers a simple but original gameplay, consisting in hunting wild creatures 
on a map with different tile types, of arbitrary dimensions (default 50x50).
Different animals have different abilities, behavioral properties and strengths, 
and after they are put down, they leave dropped items. The player has an inventory 
in his handle, which is used to store collected items from the map or default ones.
The game also disposes of a range of different items which the player can use to achieve his goals.
There are multiple weapons, as knives, axes, rifles (and bullets), and different equipments,
which protect the player and reduce the damage that animals can cause him.
Also, there is a simple level-up system, that increases the player's attack, 
defence and maximum health. The experience points are earned by putting down targets.
Also, a shopping system is to be implemented, allowing the player to buy more 
efficient items for his purposes.


Breakdown of application units

The GamePanel class contains the game loop, instantiating and handling 
all the other classes of the application. It is itself instantiated within 
the main() method of the Main class, which ensures the functioning of the game.

The Asset interface is the foundation of all the objects in the game. 
It is implemented by the Entity abstract class, that is extended by classes which 
define the player (Player), all of his possible antagonists (Monster); 
as well as the Object class, which is extended by all possible objects in the game
(weapons, equipment, drops, money, interactive tiles, etc.). The Projectile class,
extending the Entity class, is used to implement different objects that can be used as moving assets 
by different entities to damage their opponents, as the Gunshots of the hunter or the Bananas of the Gorilla.

The game mechanics are handled by such classes as CollisionChecker, which checks the
interaction of the Entities with the Objects and Tiles.

The Tile class defines the basic structure of the tiles, of which the game map is composed.

The TileManager then uses a JSON input file to generate tiles based on the indexes
(which define their nature, e.g. grass, rock, water, etc.) contained in the JSON arrays,
and the coordinates are defined by iterating the JSON object.
The generated Tiles are then stored within an array (while other implementations, 
as involving the Map interface, are to be used further); 
the whole class being fully used by the GamePanel loop, which ensures its involvement.

Analogically, the AssetManager class takes care of all objects on the map,
also uploading them from JSON files.

The SoundManager class defines the indexation of different sounds used at different 
occasions in the gameplay, which are then also used by other classes to complete their
methods.

The KeyHandler class is used to manage user input, and is instantiated within the 
GamePanel to trigger multiple events while their respective input occurs.

The UI class (stands for User Inventory) handles the objects that the player 
can collect and use for his purposes, as well as helps the player to choose some settings.



Prioritizing application components

The GamePanel class is clearly the key node of the whole application structure.
Itself being instantiated within the Main class, it handles all the processes 
of the game and ensures the correct interactions between multiple components.
It instantiates the Player class, another pinnacle of the game structure, 
being the leading subject of all the events that occur within the game.
The TileManager and AssetManager are also major nodes, as they ensure the 
content of the game is provided properly and the subject has what to interact with.
Other classes mostly define secondary methods or objects, which are instantiated 
within the above described classes.


Test levels

(Input Testing)
Selected Inputs:

Player Inventory Management
Weapon Selection and Usage
1) Player Inventory Management
   EC (Equivalence Classes) and Boundary Conditions:

Valid Inventory Actions: Adding, removing, and using items within the capacity of the inventory.
Full Inventory: Attempting to add items when inventory is full.
Invalid Actions: Trying to remove or use non-existent items.
Boundary Conditions: Testing inventory at 0 items and at maximum capacity.
Pairwise Testing Data Combinations:

Valid Add Item with Full Inventory.
Valid Remove Item with Invalid Actions.
Use Item with Non-Existent Item.
Add Item at Boundary Conditions (0 and max capacity).
2) Weapon Selection and Usage
   EC (Equivalence Classes) and Boundary Conditions:

Valid Weapon Selection: Selecting a weapon from the available inventory.
Invalid Weapon Selection: Selecting a weapon that is not in the inventory.
Weapon Usage: Using a selected weapon against a monster.
Boundary Conditions: Testing weapon usage with maximum and minimum weapon durability.
Pairwise Testing Data Combinations:

Valid Weapon Selection with Invalid Weapon Usage.
Invalid Weapon Selection with Valid Weapon Usage.
Weapon Usage with Maximum Durability.
Weapon Usage with Minimum Durability.


 (Process Testing)
Selected Processes:

Player Interaction with Monsters
Purchasing Items from Shop
1) Player Interaction with Monsters
   Process Diagram:

Encounter Monster
Trigger Battle Mode
Select Weapon/Action
Attack/Defend
Monitor Health/Experience
Conclude Battle
Process Test Scenarios:

Scenario 1: Player encounters a monster, battle initiates, and player defeats monster gaining experience.
Scenario 2: Player encounters a monster, battle initiates, player takes damage and uses an item from the inventory.
Scenario 3: Player runs out of health during battle, triggering game over sequence.
Scenario 4: Player flees from battle, returning to exploration mode.
2) Purchasing Items from Shop
   Process Diagram:

Access Shop Menu
Display Available Items
Select Item to Purchase
Confirm Purchase
Deduct Money/Add Item to Inventory
Exit Shop Menu
Process Test Scenarios:

Scenario 1: Player accesses shop, buys an item successfully, and item is added to inventory.
Scenario 2: Player tries to buy an item without enough money, and the purchase is denied with a message.
Scenario 3: Player buys an item when inventory is full, and the purchase is denied with a message.
Scenario 4: Player browses items without making a purchase and exits the shop menu.


 (Detailed Test Scenarios)
1) Detailed Test Scenario for Player Inventory Management
   Test Case ID: TC001
   Description: Verify the behavior of the system when the player attempts to add an item to a full inventory.
   Preconditions: Player inventory is at maximum capacity.
   Test Steps:

Ensure the player inventory is full.
Attempt to add a new item to the inventory.
Monitor the system response.
Verify that the system displays an appropriate error message.
Ensure the item is not added to the inventory.
Expected Results: The system should display an error message indicating that the inventory is full and prevent the item from being added.
2) Detailed Test Scenario for Weapon Selection and Usage
   Test Case ID: TC002
   Description: Verify the system’s response to the player selecting and using a weapon.
   Preconditions: Player inventory contains at least one weapon.
   Test Steps:

Navigate to the inventory screen.
Select a weapon from the inventory.
Use the weapon against a monster.
Monitor the system response.
Verify that the weapon usage is registered and affects the monster.
Ensure the weapon's durability is updated accordingly.
Expected Results: The system should allow the player to select and use the weapon, impacting the monster and updating the weapon's durability.


Summary

 (Input Testing)
Player Inventory Management: Tested for valid actions, full inventory, invalid actions, and boundary conditions.
Weapon Selection and Usage: Tested for valid and invalid selections, weapon usage, and boundary conditions of weapon durability.

(Process Testing)
Player Interaction with Monsters: Tested for initiating battle, taking damage, gaining experience, using items, and game over scenarios.
Purchasing Items from Shop: Tested for successful purchases, insufficient funds, full inventory, and browsing without purchasing.

 (Detailed Test Scenarios)
Player Inventory Management: Detailed scenario for attempting to add an item to a full inventory.
Weapon Selection and Usage: Detailed scenario for selecting and using a weapon from the inventory.