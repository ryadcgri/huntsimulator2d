package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import org.json.JSONArray;
import org.json.JSONObject;

public class MapConverter {

    public static void main(String[] args) {
        String inputFile = "src/main/resources/maps/interior01.txt"; // Change this to the path of your input text file
        String outputFile = "src/main/resources/maps/interior01.json"; // Change this to the desired output JSON file path

        try {
            // Read the map data from the text file
            BufferedReader reader = new BufferedReader(new FileReader(inputFile));
            JSONArray mapArray = new JSONArray();
            String line;
            while ((line = reader.readLine()) != null) {
                JSONArray rowArray = new JSONArray();
                String[] elements = line.split(" ");
                for (String element : elements) {
                    rowArray.put(Integer.parseInt(element));
                }
                mapArray.put(rowArray);
            }
            reader.close();

            // Create JSON object
            JSONObject mapObject = new JSONObject();
            mapObject.put("map", mapArray);

            // Write JSON object to file
            FileWriter writer = new FileWriter(outputFile);
            writer.write(mapObject.toString(4)); // Add indentation for better readability
            writer.append('\n'); // Add new line after each row
            writer.close();

            System.out.println("JSON map created successfully!");

        } catch (IOException e) {
            System.err.println("Error reading or writing file: " + e.getMessage());
            e.printStackTrace();
        }
    }
}
