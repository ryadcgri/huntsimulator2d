package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.interactive;



import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.UtilityTool;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.Object;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class Door extends Object {

    public Door(GamePanel gamePanel) {
        super(gamePanel);

        setName("Door");
        setCollision(true);

        getCollisionArea().x = 0;
        getCollisionArea().y = 16;
        getCollisionArea().width = 48;
        getCollisionArea().height = 32;
        setCollisionDefaultX(getCollisionArea().x);
        setCollisionDefaultY(getCollisionArea().y);

        try {
            BufferedImage image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/objects/door.png")));
            setImage1(UtilityTool.scaleImage(image, gamePanel.getTileSize(), gamePanel.getTileSize()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
