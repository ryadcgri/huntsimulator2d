package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.File;
import java.io.IOException;
import java.util.LinkedList;

public class JsonMapReader {

    public static LinkedList<LinkedList<Integer>> readMap(String filePath) throws IOException {
        LinkedList<LinkedList<Integer>> mapData = new LinkedList<>();

        ObjectMapper objectMapper = new ObjectMapper();
        JsonNode rootNode = objectMapper.readTree(new File(filePath));
        JsonNode mapNode = rootNode.get("map");

        for (JsonNode rowNode : mapNode) {
            LinkedList<Integer> rowList = new LinkedList<>();
            for (JsonNode tileNode : rowNode) {
                int tileNumber = tileNode.asInt();
                rowList.add(tileNumber);
            }
            mapData.add(rowList);
        }

        return mapData;
    }

    public static void main(String[] args) {
        try {
            LinkedList<LinkedList<Integer>> mapData = readMap("src/main/resources/maps/mydata.json");

            // Print the map data (just for demonstration)
            for (LinkedList<Integer> row : mapData) {
                for (int tileNumber : row) {
                    System.out.print(tileNumber + " ");
                }
                System.out.println();
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
