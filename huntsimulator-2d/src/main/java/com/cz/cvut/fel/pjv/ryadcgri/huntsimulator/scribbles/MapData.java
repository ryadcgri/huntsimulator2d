package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.scribbles;

import java.util.List;

public class MapData {
    private List<MapTileData> tilesData;

    // Getters and setters
    public List<MapTileData> getTilesData() {
        return tilesData;
    }

    public void setTilesData(List<MapTileData> tilesData) {
        this.tilesData = tilesData;
    }
}