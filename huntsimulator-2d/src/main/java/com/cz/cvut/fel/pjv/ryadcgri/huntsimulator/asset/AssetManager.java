package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset;

import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Gorilla;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.Monster;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.interactive.InteractiveTile;
import com.google.gson.Gson;
import com.google.gson.JsonObject;
import com.google.gson.JsonArray;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.MON_GreenSlime;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster.MON_Wolf;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC_Merchant;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.npc.NPC_OldMan;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment.OBJ_Axe;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment.OBJ_Shield_Blue;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.inventory.OBJ_Potion_Red;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Ammo;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Coin_Bronze;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Heart;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_ManaCrystal;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.interactive.IT_DryTree;

public class AssetManager {

    private final GamePanel gamePanel;
    private final int tileSize;
    private int map = 0;
    private JsonObject entities;

    public AssetManager(GamePanel gamePanel) {
        this.gamePanel = gamePanel;
        this.tileSize = gamePanel.getTileSize();
        loadEntities();
    }

    private void loadEntities() {
        Gson gson = new Gson();
        try (FileReader reader = new FileReader("src/main/resources/assets/entities.json")) {
            entities = gson.fromJson(reader, JsonObject.class);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setObjects() {
        map = 0;
        JsonArray objectsArray = entities.getAsJsonObject("objects").getAsJsonArray("map_0");

        for (int i = 0; i < objectsArray.size(); i++) {
            JsonObject obj = objectsArray.get(i).getAsJsonObject();
            String type = obj.get("type").getAsString();
            int worldX = obj.get("worldX").getAsInt() * tileSize;
            int worldY = obj.get("worldY").getAsInt() * tileSize;
            int index = obj.get("index").getAsInt();

            switch (type) {
                case "OBJ_Coin_Bronze":
                    gamePanel.getObjects()[map][i] = new OBJ_Coin_Bronze(gamePanel);
                    break;
                case "OBJ_Axe":
                    gamePanel.getObjects()[map][i] = new OBJ_Axe(gamePanel);
                    break;
                case "OBJ_Shield_Blue":
                    gamePanel.getObjects()[map][i] = new OBJ_Shield_Blue(gamePanel);
                    break;
                case "OBJ_Potion_Red":
                    gamePanel.getObjects()[map][i] = new OBJ_Potion_Red(gamePanel);
                    break;
                case "OBJ_Heart":
                    gamePanel.getObjects()[map][i] = new OBJ_Heart(gamePanel);
                    break;
                case "OBJ_Ammo":
                    gamePanel.getObjects()[map][i] = new OBJ_Ammo(gamePanel);
                    break;
            }
            gamePanel.getObjects()[map][i].setWorldX(worldX);
            gamePanel.getObjects()[map][i].setWorldY(worldY);
            gamePanel.getObjects()[map][i].setIndex(index);
        }
    }

    public void setNPCs() {
        JsonArray npcsArray = entities.getAsJsonObject("npcs").getAsJsonArray("map_0");
        map = 0;
        for (int i = 0; i < npcsArray.size(); i++) {
            JsonObject obj = npcsArray.get(i).getAsJsonObject();
            String type = obj.get("type").getAsString();
            int worldX = obj.get("worldX").getAsInt() * tileSize;
            int worldY = obj.get("worldY").getAsInt() * tileSize;
            int index = obj.get("index").getAsInt();

            switch (type) {
                case "NPC_OldMan":
                    gamePanel.getNpcs()[map][i] = new NPC_OldMan(gamePanel);
                    break;
                case "NPC_Merchant":
                    gamePanel.getNpcs()[map][i] = new NPC_Merchant(gamePanel);
                    break;
            }
            gamePanel.getNpcs()[map][i].setWorldX(worldX);
            gamePanel.getNpcs()[map][i].setWorldY(worldY);
            gamePanel.getNpcs()[map][i].setIndex(index);
        }

        npcsArray = entities.getAsJsonObject("npcs").getAsJsonArray("map_1");
        map = 1;
        for (int i = 0; i < npcsArray.size(); i++) {
            JsonObject obj = npcsArray.get(i).getAsJsonObject();
            String type = obj.get("type").getAsString();
            int worldX = obj.get("worldX").getAsInt() * tileSize;
            int worldY = obj.get("worldY").getAsInt() * tileSize;
            int index = obj.get("index").getAsInt();

            switch (type) {
                case "NPC_OldMan":
                    gamePanel.getNpcs()[map][i] = new NPC_OldMan(gamePanel);
                    break;
                case "NPC_Merchant":
                    gamePanel.getNpcs()[map][i] = new NPC_Merchant(gamePanel);
                    break;
            }
            gamePanel.getNpcs()[map][i].setWorldX(worldX);
            gamePanel.getNpcs()[map][i].setWorldY(worldY);
            gamePanel.getNpcs()[map][i].setIndex(index);
        }
    }

    public void setMonsters() {
        map = 0;
        JsonArray monstersArray = entities.getAsJsonObject("monsters").getAsJsonArray("map_0");

        for (int i = 0; i < monstersArray.size(); i++) {
            JsonObject obj = monstersArray.get(i).getAsJsonObject();
            String type = obj.get("type").getAsString();
            int worldX = obj.get("worldX").getAsInt() * tileSize;
            int worldY = obj.get("worldY").getAsInt() * tileSize;
            int index = obj.get("index").getAsInt();

            switch (type) {
                case "MON_GreenSlime":
                    gamePanel.getMonsters()[map][i] = new MON_GreenSlime(gamePanel);
                    break;
                case "MON_Wolf":
                    gamePanel.getMonsters()[map][i] = new MON_Wolf(gamePanel);
                    break;
                case "Gorilla":
                    gamePanel.getMonsters()[map][i] = new Gorilla(gamePanel);
                    break;
            }
            gamePanel.getMonsters()[map][i].setWorldX(worldX);
            gamePanel.getMonsters()[map][i].setWorldY(worldY);
            gamePanel.getMonsters()[map][i].setIndex(index);
        }
    }

    public void setInteractiveTiles() {
        map = 0;
        JsonArray tilesArray = entities.getAsJsonObject("interactiveTiles").getAsJsonArray("map_0");

        for (int i = 0; i < tilesArray.size(); i++) {
            JsonObject obj = tilesArray.get(i).getAsJsonObject();
            String type = obj.get("type").getAsString();
            int worldX = obj.get("worldX").getAsInt() * tileSize;
            int worldY = obj.get("worldY").getAsInt() * tileSize;
            int index = obj.get("index").getAsInt();

            switch (type) {
                case "IT_DryTree":
                    gamePanel.getInteractiveTiles()[map][i] = new IT_DryTree(gamePanel);
                    break;
            }
            gamePanel.getInteractiveTiles()[map][i].setWorldX(worldX);
            gamePanel.getInteractiveTiles()[map][i].setWorldY(worldY);
            gamePanel.getInteractiveTiles()[map][i].setIndex(index);
        }
    }

    public void saveEntities() {
        Gson gson = new Gson();
        JsonObject entitiesToSave = new JsonObject();

        // Save objects
        JsonObject objects = new JsonObject();
        JsonArray map0Objects = new JsonArray();
        for (int i = 0; i < gamePanel.getObjects()[0].length; i++) {
            Asset obj = gamePanel.getObjects()[0][i];
            if (obj != null) {
                JsonObject objJson = new JsonObject();
                objJson.addProperty("type", obj.getClass().getSimpleName());
                objJson.addProperty("worldX", obj.getWorldX() / tileSize);
                objJson.addProperty("worldY", obj.getWorldY() / tileSize);
                objJson.addProperty("index", i);
                map0Objects.add(objJson);
            }
        }
        objects.add("map_0", map0Objects);
        entitiesToSave.add("objects", objects);

        // Save NPCs
        JsonObject npcs = new JsonObject();
        JsonArray map0Npcs = new JsonArray();
        for (int i = 0; i < gamePanel.getNpcs()[0].length; i++) {
            NPC npc = (NPC) gamePanel.getNpcs()[0][i];
            if (npc != null) {
                JsonObject npcJson = new JsonObject();
                npcJson.addProperty("type", npc.getClass().getSimpleName());
                npcJson.addProperty("worldX", npc.getWorldX() / tileSize);
                npcJson.addProperty("worldY", npc.getWorldY() / tileSize);
                npcJson.addProperty("index", i);
                map0Npcs.add(npcJson);
            }
        }
        npcs.add("map_0", map0Npcs);

        JsonArray map1Npcs = new JsonArray();
        // Save NPCs (continued)
        for (int i = 0; i < gamePanel.getNpcs()[1].length; i++) {
            NPC npc = (NPC) gamePanel.getNpcs()[1][i];
            if (npc != null) {
                JsonObject npcJson = new JsonObject();
                npcJson.addProperty("type", npc.getClass().getSimpleName());
                npcJson.addProperty("worldX", npc.getWorldX() / tileSize);
                npcJson.addProperty("worldY", npc.getWorldY() / tileSize);
                npcJson.addProperty("index", i);
                map1Npcs.add(npcJson);
            }
        }
        npcs.add("map_1", map1Npcs);
        entitiesToSave.add("npcs", npcs);

        // Save monsters
        JsonObject monsters = new JsonObject();
        JsonArray map0Monsters = new JsonArray();
        for (int i = 0; i < gamePanel.getMonsters()[0].length; i++) {
            Asset monster = gamePanel.getMonsters()[0][i];
            if (monster != null) {
                JsonObject monsterJson = new JsonObject();
                monsterJson.addProperty("type", monster.getClass().getSimpleName());
                monsterJson.addProperty("worldX", monster.getWorldX() / tileSize);
                monsterJson.addProperty("worldY", monster.getWorldY() / tileSize);
                monsterJson.addProperty("index", i);
                map0Monsters.add(monsterJson);
            }
        }
        monsters.add("map_0", map0Monsters);
        entitiesToSave.add("monsters", monsters);

        // Save interactive tiles
        JsonObject interactiveTiles = new JsonObject();
        JsonArray map0Tiles = new JsonArray();
        for (int i = 0; i < gamePanel.getInteractiveTiles()[0].length; i++) {
            InteractiveTile tile = gamePanel.getInteractiveTiles()[0][i];
            if (tile != null) {
                JsonObject tileJson = new JsonObject();
                tileJson.addProperty("type", tile.getClass().getSimpleName());
                tileJson.addProperty("worldX", tile.getWorldX() / tileSize);
                tileJson.addProperty("worldY", tile.getWorldY() / tileSize);
                tileJson.addProperty("index", i);
                map0Tiles.add(tileJson);
            }
        }
        interactiveTiles.add("map_0", map0Tiles);
        entitiesToSave.add("interactiveTiles", interactiveTiles);

        // Write JSON to file
        File file = new File("src/main/resources/assets/entities_saved.json");
        try (FileWriter writer = new FileWriter(file)) {
            if (file.exists()) {
                // Clear existing file content
                writer.write("");
            }
            gson.toJson(entitiesToSave, writer);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public JsonObject getEntities(){
        return entities;
    }



}
