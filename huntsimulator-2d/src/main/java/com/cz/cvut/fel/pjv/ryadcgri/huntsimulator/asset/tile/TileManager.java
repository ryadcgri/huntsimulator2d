package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile;

import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.UtilityTool;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import javax.imageio.ImageIO;
import java.awt.*;
import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.Objects;

public class TileManager {

    private final GamePanel gamePanel;
    private final Tile[] tiles;
    private final int[][][] mapTileNumbers;

    public TileManager(GamePanel gamePanel, int dimensions) {
        this.gamePanel = gamePanel;

        this.tiles = new Tile[dimensions];
        this.mapTileNumbers = new int[gamePanel.getMaxMaps()][gamePanel.getMaxWorldColumns()][gamePanel.getMaxWorldRows()];

        getTileImage();
        loadMapFromJson("/maps/worldinjson.json", 0);
        loadMapFromJson("/maps/interior01.json", 1);
    }

    public void getTileImage() {
        // PLACEHOLDER
        setup(0, "grass", false);
        setup(1, "grass", false);
        setup(2, "grass", false);
        setup(3, "grass", false);
        setup(4, "grass", false);
        setup(5, "grass", false);
        setup(6, "grass", false);
        setup(7, "grass", false);
        setup(8, "grass", false);
        setup(9, "grass", false);
        // PLACEHOLDER

        // TILES LOAD IN
        setup(10, "grass", false);
        setup(11, "grass", false);
        setup(12, "water", true);
        setup(13, "water", true);
        setup(14, "water", true);
        setup(15, "water", true);
        setup(16, "water", true);
        setup(17, "water", true);
        setup(18, "water", true);
        setup(19, "water", true);
        setup(20, "water", true);
        setup(21, "water", true);
        setup(22, "water", true);
        setup(23, "water", true);
        setup(24, "water", true);
        setup(25, "water", true);
        setup(26, "sand", false);
        setup(27, "sand", false);
        setup(28, "sand", false);
        setup(29, "sand", false);
        setup(30, "sand", false);
        setup(31, "sand", false);
        setup(32, "sand", false);
        setup(33, "sand", false);
        setup(34, "sand", false);
        setup(35, "sand", false);
        setup(36, "sand", false);
        setup(37, "sand", false);
        setup(38, "sand", false);
        setup(39, "earth", false);
        setup(40, "rock", true);
        setup(41, "tree", true);
        setup(42, "hut", false);
        setup(43, "floor01", false);
        setup(44, "table01", true);
    }

    public void setup(int index, String imageName, boolean collision) {
        try {
            tiles[index] = new Tile();
            tiles[index].setImage(ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/tiles/" + imageName + ".png"))));
            tiles[index].setImage(UtilityTool.scaleImage(tiles[index].getImage(), gamePanel.getTileSize(), gamePanel.getTileSize()));
            tiles[index].setCollision(collision);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadMapFromJson(String mapPath, int map) {
        try {
            InputStream inputStream = getClass().getResourceAsStream(mapPath);
            ObjectMapper objectMapper = new ObjectMapper();
            JsonNode rootNode = objectMapper.readTree(inputStream);
            JsonNode mapNode = rootNode.get("map");

            int row = 0;

            for (JsonNode rowNode : mapNode) {
                int column = 0;
                for (JsonNode tileNode : rowNode) {
                    int tileNumber = tileNode.asInt();
                    mapTileNumbers[map][column][row] = tileNumber;
                    column++;
                }
                row++;
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void draw(Graphics2D graphics2D) {
        int worldColumn = 0;
        int worldRow = 0;

        while (worldColumn < gamePanel.getMaxWorldColumns() && worldRow < gamePanel.getMaxWorldRows()) {

            int tileNumber = mapTileNumbers[gamePanel.getCurrentMap()][worldColumn][worldRow];

            int worldX = worldColumn * gamePanel.getTileSize();
            int worldY = worldRow * gamePanel.getTileSize();
            int screenX = worldX - gamePanel.getPlayer().getWorldX() + gamePanel.getPlayer().getScreenX();
            int screenY = worldY - gamePanel.getPlayer().getWorldY() + gamePanel.getPlayer().getScreenY();

            // Stop moving the camera at world edge
            int rightOffset = gamePanel.getScreenWidth() - gamePanel.getPlayer().getScreenX();
            screenX = checkIfAtEdgeOfXAxis(worldX, screenX, rightOffset);

            int bottomOffset = gamePanel.getScreenHeight() - gamePanel.getPlayer().getScreenY();
            screenY = checkIfAtEdgeOfYAxis(worldY, screenY, bottomOffset);

            if (UtilityTool.isInsidePlayerView(worldX, worldY, gamePanel)) {
                graphics2D.drawImage(tiles[tileNumber].getImage(), screenX, screenY, null);

            } else if (gamePanel.getPlayer().getScreenX() > gamePanel.getPlayer().getWorldX()
                    || gamePanel.getPlayer().getScreenY() > gamePanel.getPlayer().getWorldY()
                    || rightOffset > gamePanel.getWorldWidth() - gamePanel.getPlayer().getWorldX()
                    || bottomOffset > gamePanel.getWorldHeight() - gamePanel.getPlayer().getWorldY()) {
                graphics2D.drawImage(tiles[tileNumber].getImage(), screenX, screenY, null);
            }

            worldColumn++;

            if (worldColumn == gamePanel.getMaxWorldColumns()) {
                worldColumn = 0;
                worldRow++;
            }
        }
    }

    private int checkIfAtEdgeOfXAxis(int worldX, int screenX, int rightOffset) {
        if (gamePanel.getPlayer().getScreenX() > gamePanel.getPlayer().getWorldX()) {
            return worldX;
        }

        if (rightOffset > gamePanel.getWorldWidth() - gamePanel.getPlayer().getWorldX()) {
            return gamePanel.getScreenWidth() - (gamePanel.getWorldWidth() - worldX);
        }

        return screenX;
    }

    private int checkIfAtEdgeOfYAxis(int worldY, int screenY, int bottomOffset) {
        if (gamePanel.getPlayer().getScreenY() > gamePanel.getPlayer().getWorldY()) {
            return worldY;
        }

        if (bottomOffset > gamePanel.getWorldHeight() - gamePanel.getPlayer().getWorldY()) {
            return gamePanel.getScreenHeight() - (gamePanel.getWorldHeight() - worldY);
        }

        return screenY;
    }

    public Tile[] getTiles() {
        return tiles;
    }

    public int[][][] getMapTileNumbers() {
        return mapTileNumbers;
    }
}
