package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.UtilityTool;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.Object;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class Rifle extends Weapon {

    public Rifle(GamePanel gamePanel) {
        super(gamePanel);

        setName("Rifle");
        setDescription("[" + getName() + "]\nA must-have for \na hunter. \nShoots with bullets.");
        setAttackValue(1);
        getAttackArea().width = 36;
        getAttackArea().height = 36;
        setPrice(20);

        try {
            BufferedImage image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/objects/rifle.png")));
            setImage1(UtilityTool.scaleImage(image, gamePanel.getTileSize(), gamePanel.getTileSize()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
