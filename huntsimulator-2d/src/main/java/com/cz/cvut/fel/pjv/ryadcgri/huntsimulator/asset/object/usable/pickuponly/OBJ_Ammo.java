package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly;




import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.UtilityTool;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class OBJ_Ammo extends PickUpOnlyObject {

    private final GamePanel gamePanel;

    public OBJ_Ammo(GamePanel gamePanel) {
        super(gamePanel);
        this.gamePanel = gamePanel;

        setName("Rifle ammo");
        setValue(1);
        setDescription("[" + getName() + "]\nWill add " + getValue() + " ammo");

        try {
            BufferedImage image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/objects/bullets-full.png")));
            setImage1(UtilityTool.scaleImage(image, gamePanel.getTileSize(), gamePanel.getTileSize()));

            image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/objects/bullets-blank.png")));
            setImage2(UtilityTool.scaleImage(image, gamePanel.getTileSize(), gamePanel.getTileSize()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void use() {
        gamePanel.playSoundEffect(1);
        gamePanel.getUi().addMessage("Ammo +" + getValue());
        gamePanel.getPlayer().setCurrentMana(gamePanel.getPlayer().getCurrentMana() + getValue());
    }
}
