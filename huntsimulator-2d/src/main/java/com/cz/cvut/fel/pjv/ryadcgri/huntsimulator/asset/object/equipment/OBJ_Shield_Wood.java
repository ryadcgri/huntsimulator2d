package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.equipment;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.UtilityTool;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;


public class OBJ_Shield_Wood extends Shield {

    public OBJ_Shield_Wood(GamePanel gamePanel) {
        super(gamePanel);

        setName("Basic equipment");
        setDescription("[" + getName() + "]\nProtects you \nin a decent way.");
        setDefenseValue(1);
        setPrice(35);

        try {
            BufferedImage image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/objects/basic-equipment.png")));
            setImage1(UtilityTool.scaleImage(image, gamePanel.getTileSize(), gamePanel.getTileSize()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
