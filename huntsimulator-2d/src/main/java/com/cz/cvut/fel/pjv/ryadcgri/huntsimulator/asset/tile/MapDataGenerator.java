package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile;

import org.json.JSONArray;
import org.json.JSONObject;

import java.io.FileWriter;
import java.io.IOException;

public class MapDataGenerator {

    public static void main(String[] args) {
        JSONObject mapData = new JSONObject();
        JSONArray tilesData = new JSONArray();

        // Generate tile data
        for (int y = 0; y < 50; y++) {
            for (int x = 0; x < 50; x++) {
                JSONObject tileData = new JSONObject();
                tileData.put("type", getType(x, y));
                tileData.put("x", x);
                tileData.put("y", y);
                tilesData.put(tileData);
            }
        }

        mapData.put("tilesData", tilesData);

        // Write JSON data to file
        try (FileWriter file = new FileWriter("mapData.json")) {
            file.write(mapData.toString());
            System.out.println("JSON data written to mapData.json successfully!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Method to determine tile type based on coordinates
    private static int getType(int x, int y) {
        if ((x + y) % 3 == 0) {
            return 0; // Grass
        } else if ((x + y) % 3 == 1) {
            return 1; // Water
        } else {
            return 2; // Sand
        }
    }
}
