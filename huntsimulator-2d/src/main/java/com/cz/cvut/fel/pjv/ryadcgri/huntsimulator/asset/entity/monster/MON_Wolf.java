package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.monster;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.ability.OBJ_Rock;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.inventory.OBJ_Meat;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Ammo;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Coin_Bronze;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_Heart;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly.OBJ_ManaCrystal;

import java.awt.*;
import java.util.Objects;
import java.util.Random;

public class MON_Wolf extends Monster{


    public MON_Wolf(GamePanel gamePanel) {
        super(gamePanel);

        setName("Wolf");
        setDirection("down");
        setSpeed(1);
        setMaxLife(8);
        setCurrentLife(getMaxLife());
        setAttackPower(5);
        setDefensePower(0);
        setExp(2);

        setProjectile(new OBJ_Rock(gamePanel));
        setMaxAmmo(0);
        setCurrentAmmo(getMaxAmmo());

        setCollisionArea(new Rectangle(3, 18, 42, 30));
        setCollisionDefaultX(getCollisionArea().x);
        setCollisionDefaultY(getCollisionArea().y);

        getAnimationImages();
    }

    public void getAnimationImages() {
        setUp1(setup("/images/monster/greenslime_down_1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setUp2(setup("/images/monster/greenslime_down_2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setDown1(setup("/images/monster/greenslime_down_1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setDown2(setup("/images/monster/greenslime_down_2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setLeft1(setup("/images/monster/greenslime_down_1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setLeft2(setup("/images/monster/greenslime_down_2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setRight1(setup("/images/monster/greenslime_down_1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setRight2(setup("/images/monster/greenslime_down_2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
    }

    @Override
    public void damageReaction() {
        setActionLockCounter(0);
        //setDirection(getGamePanel().getPlayer().getDirection());
        //getGamePanel().getPlayer().getDirection();
        if (Objects.equals(getGamePanel().getPlayer().getDirection(), "up")){
            setDirection("down");
        }else if (Objects.equals(getGamePanel().getPlayer().getDirection(), "down")){
            setDirection("up");
        }else if (Objects.equals(getGamePanel().getPlayer().getDirection(), "right")){
            setDirection("left");
        } else if (Objects.equals(getGamePanel().getPlayer().getDirection(), "left")){
            setDirection("right");
        }

    }

    @Override
    public void setupAI() {
        super.setupAI();
    }

    @Override
    public void checkDrop() {
        int i = new Random().nextInt(100) + 1;

        dropObject(new OBJ_Meat(getGamePanel()));

        if (i < 50) {
            dropObject(new OBJ_Coin_Bronze(getGamePanel()));
        }

        if (i >= 50 && i < 75) {
            dropObject(new OBJ_Heart(getGamePanel()));
        }

        if (i >= 75 && i < 100) {
            dropObject(new OBJ_Ammo(getGamePanel()));
        }
    }

}
