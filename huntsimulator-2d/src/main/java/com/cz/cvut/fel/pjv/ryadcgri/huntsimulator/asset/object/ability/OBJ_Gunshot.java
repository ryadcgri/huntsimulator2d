package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.ability;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.Entity;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.entity.ability.Projectile;

import java.awt.*;

public class OBJ_Gunshot extends Projectile {

    public OBJ_Gunshot(GamePanel gamePanel) {
        super(gamePanel);

        setName("Gunshot");
        setSpeed(6);
        setMaxLife(80);
        setCurrentLife(getMaxLife());
        setAttackPower(4);
        setUseCost(1);
        setAlive(false);

        getAnimationImages();
    }

    public void getAnimationImages() {
        setUp1(setup("/images/ability/bullet-up-1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setUp2(setup("/images/ability/bullet-up-2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setDown1(setup("/images/ability/bullet-down-1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setDown2(setup("/images/ability/bullet-down-2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setLeft1(setup("/images/ability/bullet-left-1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setLeft2(setup("/images/ability/bullet-left-2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setRight1(setup("/images/ability/bullet-right-1", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
        setRight2(setup("/images/ability/bullet-right-2", getGamePanel().getTileSize(), getGamePanel().getTileSize()));
    }

    @Override
    public boolean haveEnoughResource(Entity user) {
        return user.getCurrentMana() >= getUseCost();
    }

    @Override
    public void subtractResource(Entity user) {
        user.setCurrentMana(user.getCurrentMana() - getUseCost());
    }

    @Override
    public Color getParticleColor() {
        return new Color(240, 50, 0);
    }

    @Override
    public int getParticleSize() {
        return 10; // pixels
    }

    @Override
    public int getParticleSpeed() {
        return 1;
    }

    @Override
    public int getParticleMaxLife() {
        return 20; // How long it will last
    }
}
