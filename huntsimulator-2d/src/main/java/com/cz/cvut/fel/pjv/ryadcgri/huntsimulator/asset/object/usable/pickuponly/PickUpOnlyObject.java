package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.pickuponly;


import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.Object;

public class PickUpOnlyObject extends Object {

    public PickUpOnlyObject(GamePanel gamePanel) {
        super(gamePanel);
    }
}
