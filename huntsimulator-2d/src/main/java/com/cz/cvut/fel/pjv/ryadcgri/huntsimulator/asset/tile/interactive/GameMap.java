package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.tile.interactive;

import org.json.JSONArray;
import org.json.JSONObject;

public class GameMap {

    private final JSONObject mapData;

    public GameMap(JSONObject mapData) {
        this.mapData = mapData;
    }

    public int getTileAt(int column, int row) {
        JSONArray columns = mapData.getJSONArray("tiles");
        if (column >= 0 && column < columns.length()) {
            JSONArray rows = columns.getJSONArray(column);
            if (row >= 0 && row < rows.length()) {
                return rows.getInt(row);
            }
        }
        // Default to -1 if the tile is out of bounds
        return -1;
    }

    public int getWidth() {
        JSONArray columns = mapData.getJSONArray("tiles");
        return columns.length();
    }

    public int getHeight() {
        JSONArray columns = mapData.getJSONArray("tiles");
        if (columns.length() > 0) {
            JSONArray rows = columns.getJSONArray(0);
            return rows.length();
        }
        return 0;
    }

    // Add more methods as needed to access different aspects of the map data
}
