package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.scribbles;

public class MapTileData {
    private int type;
    private int x;
    private int y;

    // Getters and setters
    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }
}