package com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.usable.inventory;



import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.GamePanel;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.util.UtilityTool;
import com.cz.cvut.fel.pjv.ryadcgri.huntsimulator.asset.object.Object;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Objects;

public class OBJ_Meat extends Object {

    private final GamePanel gamePanel;

    public OBJ_Meat(GamePanel gamePanel) {
        super(gamePanel);
        this.gamePanel = gamePanel;

        setName("Meat");
        setValue(1);
        setDescription("[" + getName() + "]\nRestores " + getValue() + " health");
        setPrice(5);

        try {
            BufferedImage image = ImageIO.read(Objects.requireNonNull(getClass().getResourceAsStream("/images/objects/meat.png")));
            setImage1(UtilityTool.scaleImage(image, gamePanel.getTileSize(), gamePanel.getTileSize()));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void use() {
        gamePanel.setGameState(gamePanel.getDialogueState());
        gamePanel.getUi().setCurrentDialogue("You eat " + getName() + "!\n" +
                "You have restored " + getValue() + " life!");

        gamePanel.getPlayer().setCurrentLife(gamePanel.getPlayer().getCurrentLife() + getValue());

        gamePanel.playSoundEffect(13);
    }
}
